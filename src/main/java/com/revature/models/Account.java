package com.revature.models;

public class Account {
	
	private double balance;
	private int accountNumber;
	private String typeOfAccount;
	
	
	
	
	public Account(double balance, int accountNumber, String typeOfAccount) {
		super();
		this.balance = balance;
		this.accountNumber = accountNumber;
		this.typeOfAccount = typeOfAccount;
	}

	public String getTypeOfAccount() {
		return typeOfAccount;
	}

	public void setTypeOfAccount(String typeOfAccount) {
		this.typeOfAccount = typeOfAccount;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	
	public double withdraw(double amount) {
		this.balance -= amount;
		return balance;
	}

	
	public Account() {
		super();
	}

	public double deposit(double amount) {
		this.balance += amount;
		return balance;
	}

}
