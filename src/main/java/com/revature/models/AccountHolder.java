package com.revature.models;

public class AccountHolder {

	private String name;
	private int accountId;

	public AccountHolder() {
		super();
	}

	public AccountHolder(String name, int accountNumber) {
		super();
		this.name = name;
		this.accountId = accountNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAccountNumber() {
		return accountId;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountId = accountNumber;
	}

}
