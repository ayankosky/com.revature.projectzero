package com.revature.controllers;

import java.sql.SQLException;
import io.javalin.Javalin;
import io.javalin.http.Context;

import com.revature.dao.AccountHolderDAO;
import com.revature.exceptions.NoSQLResultsException;
import com.revature.models.AccountHolder;
import com.revature.utils.ConnectionFactory;

public class AccountHolderController {

	public static void getById(Context ctx)  {
		AccountHolderDAO dao = new AccountHolderDAO(ConnectionFactory.getConnection());
		int id = Integer.parseInt(ctx.pathParam("id"));
		try {
			ctx.json(dao.get(id));
			ctx.status(201);
		} catch (SQLException e) {
			ctx.status(404);
			e.printStackTrace();
		} catch (NoSQLResultsException e) {
			ctx.status(404);
			e.printStackTrace();
		}

	}

	public static void getAll(Context ctx) {
		AccountHolderDAO dao = new AccountHolderDAO(ConnectionFactory.getConnection());

		try {
			ctx.json(dao.getAll());
		} catch (SQLException e) {
			ctx.status(404);
			e.printStackTrace();
		} catch (NoSQLResultsException e) {
			ctx.status(404);
			e.printStackTrace();
		}
	}

	public static void add(Context ctx) {
		AccountHolderDAO dao = new AccountHolderDAO(ConnectionFactory.getConnection());
		AccountHolder ah = ctx.bodyAsClass(AccountHolder.class);
		try {
			dao.add(ah);
		} catch (SQLException e) {
			ctx.status(404);
			e.printStackTrace();
		} catch (NoSQLResultsException e) {
			ctx.status(404);
			e.printStackTrace();
		}
	}

	public static void update(Context ctx) {
		AccountHolderDAO dao = new AccountHolderDAO(ConnectionFactory.getConnection());
		AccountHolder ah = ctx.bodyAsClass(AccountHolder.class);
		int id = Integer.parseInt(ctx.pathParam("id"));
		try {
			dao.update(ah, id);
		} catch (SQLException e) {
			ctx.status(404);
			e.printStackTrace();
		} catch (NoSQLResultsException e) {
			ctx.status(404);
			e.printStackTrace();
		}
	}

	public static void delete(Context ctx)  {
		AccountHolderDAO dao = new AccountHolderDAO(ConnectionFactory.getConnection());
		int id = Integer.parseInt(ctx.pathParam("id"));
		try {
			ctx.status(205);
			dao.delete(id);

		} catch (SQLException e) {
			ctx.status(404);
			e.printStackTrace();
		} catch (NoSQLResultsException e) {
			ctx.status(404);
			e.printStackTrace();
		}
	}

}
