package com.revature.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionFactory {

	private static Connection connection;

	public static Connection getConnection() {
		if (connection == null) {
			try {
				Properties prop = new Properties();
				FileReader fr = new FileReader("src/main/resources/connection.properties");
				prop.load(fr);

				String connString = "jdbc:mariadb://" + prop.getProperty("endpoint") + ":" + prop.getProperty("port")
						+ "/" + prop.getProperty("dbname") + "?user=" + prop.getProperty("user") + "&password="
						+ prop.getProperty("password");

				connection = DriverManager.getConnection(connString);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return connection;
	}
}
