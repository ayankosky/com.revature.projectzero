package com.revature.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.revature.exceptions.NoSQLResultsException;
import com.revature.models.AccountHolder;
import com.revature.utils.ConnectionFactory;

public class AccountHolderDAO {

	private ArrayList<AccountHolder> accountHolders = new ArrayList<AccountHolder>();
	Connection connection;

	public AccountHolderDAO(Connection conn) {

		connection = conn;
	}

	public AccountHolder get(int id) throws SQLException, NoSQLResultsException {
		String sql = "Select * From accountholders Where accountholderid = ?";
		PreparedStatement pstmt = connection.prepareStatement(sql);
		pstmt.setInt(1, id);
		ResultSet rs = pstmt.executeQuery();
		rs.first();
		AccountHolder ah = new AccountHolder();
		ah.setAccountNumber(rs.getInt("accountHolderId"));
		ah.setName(rs.getString("name"));
		return ah;

	}

	public List getAll() throws SQLException, NoSQLResultsException {
		String sql = "Select * From accountholders";
		PreparedStatement pstmt = connection.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();

		accountHolders = new ArrayList<AccountHolder>();

		while (rs.next()) {
			AccountHolder row = new AccountHolder();
			row.setAccountNumber(rs.getInt("AccountHolderId"));
			row.setName(rs.getString("Name"));
			accountHolders.add(row);
		}

		return accountHolders;
	}

	public void add(AccountHolder accountHolder) throws SQLException, NoSQLResultsException {
		String sql = "Insert into accountholders (name) values (?)";
		PreparedStatement pstmt = connection.prepareStatement(sql);
		pstmt.setString(1, accountHolder.getName());
		pstmt.executeUpdate();
		pstmt.getResultSet();
	}

	public void update(AccountHolder ah, int id) throws SQLException, NoSQLResultsException {
		String sql = "Update accountholders set name = (?) Where accountholderid = (?)";
		PreparedStatement pstmt = connection.prepareStatement(sql);
		pstmt.setString(1, ah.getName());
		pstmt.setInt(2, id);
		int result = pstmt.executeUpdate();
		if (result == 0) {
			throw new NoSQLResultsException();
		}
	}

	public void delete(int id) throws SQLException, NoSQLResultsException {
		String sql = "delete from accountholders where AccountHolderID = (?)";
		PreparedStatement pstmt = connection.prepareStatement(sql);
		pstmt.setInt(1, id);

		int result = pstmt.executeUpdate();
		System.out.println(result);
		if (result == 0) {
			throw new NoSQLResultsException();
		}
	}

}
