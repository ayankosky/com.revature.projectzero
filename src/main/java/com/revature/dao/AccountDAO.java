package com.revature.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.revature.exceptions.InsufficentFundsException;
import com.revature.exceptions.NoSQLResultsException;
import com.revature.models.Account;
import com.revature.models.AccountHolder;

import io.javalin.http.Context;

public class AccountDAO {
	Account account;
	private ArrayList<Account> accounts;
	Connection connection;

	public AccountDAO(Connection conn) {
		connection = conn;
	}

	public ArrayList<Account> getAccounts(int id) throws SQLException, NoSQLResultsException {
		accounts = new ArrayList<Account>();
		String sql = "Select * From accounts WHERE accountHolderID = (?)";
		PreparedStatement pstmt = connection.prepareStatement(sql);
		pstmt.setInt(1, id);
		ResultSet rs = pstmt.executeQuery();

		accounts = new ArrayList<Account>();

		while (rs.next()) {

			Account row = new Account();
			row.setAccountNumber(rs.getInt("AccountHolderId"));
			row.setBalance(rs.getDouble("Balance"));
			row.setTypeOfAccount(rs.getString("AccountType"));
			accounts.add(row);
		}

		return accounts;
	}

	public void addAccount(int id, Account account) throws SQLException, NoSQLResultsException {
		String sql = "Select * From accountholders WHERE accountHolderID = (?)";
		PreparedStatement pstmt = connection.prepareStatement(sql);
		pstmt.setInt(1, id);
		ResultSet rs = pstmt.executeQuery();
		if (rs != null) {
			sql = "insert into accounts (accountHolderID, Balance, AccountType) values ((?), (?), (?))";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.setDouble(2, account.getBalance());
			pstmt.setString(3, account.getTypeOfAccount());
			pstmt.executeUpdate();
			pstmt.getResultSet();
		}
	}

	public Account getAcc(int id, int accountNum) throws SQLException, NoSQLResultsException {
		String sql = "select * from accounts where accountholderid = (?) and accountid = (?)";
		PreparedStatement pstmt = connection.prepareStatement(sql);
		pstmt.setInt(1, id);
		pstmt.setInt(2, accountNum);
		ResultSet rs = pstmt.executeQuery();
			rs.first();
			Account account = new Account();

			account.setTypeOfAccount(rs.getString("AccountType"));

			account.setBalance(rs.getDouble("Balance"));

			account.setAccountNumber(rs.getInt("AccountID"));

			return account;
		

	}

	public ArrayList<Account> getAccountRange(int id, int top, int bot) throws SQLException, NoSQLResultsException {

		String sql = "select * from accounts where accountholderid = (?) and balance < (?) and balance > (?)";

		PreparedStatement pstmt = connection.prepareStatement(sql);
		pstmt.setInt(1, id);
		pstmt.setInt(2, top);
		pstmt.setInt(3, bot);
		ResultSet rs = pstmt.executeQuery();

		accounts = new ArrayList<Account>();
		while (rs.next()) {

			Account row = new Account();
			row.setTypeOfAccount(rs.getString("AccountType"));
			row.setAccountNumber(rs.getInt("AccountHolderId"));
			row.setBalance(rs.getDouble("Balance"));
			accounts.add(row);

		}
		return accounts;

	}

	public void updateType(Account account, int id, int accId) throws SQLException, NoSQLResultsException {
		String sql = "Update accounts set AccountType = (?) Where accountholderid = (?) and accountID = (?)";
		PreparedStatement pstmt = connection.prepareStatement(sql);
		pstmt.setString(1, account.getTypeOfAccount());
		pstmt.setInt(2, id);
		pstmt.setInt(3, accId);
		int result = pstmt.executeUpdate();
		if(result == 0) {
			throw new NoSQLResultsException();
		}

	}

	public void deleteAcc(int id, int accId) throws SQLException, NoSQLResultsException {
		String sql = "Select name from accountholders where accountholderId = (?)";
		PreparedStatement pstmt = connection.prepareStatement(sql);
		pstmt.setInt(1, id);
		ResultSet rs = pstmt.executeQuery();
		if (rs != null) {
			sql = "Select * from accounts where accountId = (?)";
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, accId);
			rs = pstmt.executeQuery();
			if (rs != null) {
				sql = "delete from accounts where AccountHolderID = (?) and AccountID = (?)";
				pstmt = connection.prepareStatement(sql);
				pstmt.setInt(1, id);
				pstmt.setInt(2, accId);
				int result = pstmt.executeUpdate();
				if(result == 0) {
					throw new NoSQLResultsException();
				}
			}
		}

	}

	public void deposit(int id, int accId, double amount) throws SQLException, NoSQLResultsException {
		Account account = new Account();
		String sql = "Select * from accounts where accountholderId = (?) and accountid = (?)";
		PreparedStatement pstmt = connection.prepareStatement(sql);
		pstmt.setInt(1, id);
		pstmt.setInt(2, accId);
		ResultSet rs = pstmt.executeQuery();
		if (rs.first()) {
			account.setBalance(rs.getDouble("Balance"));
			account.deposit(amount);
			sql = "Update accounts set Balance = (?) Where accountholderid = (?) and accountID = (?)";
			pstmt = connection.prepareStatement(sql);
			pstmt.setDouble(1, account.getBalance());
			pstmt.setInt(2, id);
			pstmt.setInt(3, accId);
			pstmt.executeQuery();
		}

	}

	public void withdraw(int id, int accId, double amount)
			throws SQLException, InsufficentFundsException, NoSQLResultsException {
		Account account = new Account();
		String sql = "Select * from accounts where accountholderId = (?) and accountid = (?)";
		PreparedStatement pstmt = connection.prepareStatement(sql);
		pstmt.setInt(1, id);
		pstmt.setInt(2, accId);
		ResultSet rs = pstmt.executeQuery();
		if (rs.first()) {
			account.setBalance(rs.getDouble("Balance"));
			if (account.getBalance() - amount > 0) {
				account.withdraw(amount);
				sql = "Update accounts set Balance = (?) Where accountholderid = (?) and accountID = (?)";
				pstmt = connection.prepareStatement(sql);
				pstmt.setDouble(1, account.getBalance());
				pstmt.setInt(2, id);
				pstmt.setInt(3, accId);
				pstmt.executeQuery();
			} else
				throw new InsufficentFundsException();

		}
	}

	public void transfer(int id, int accId, int transferId, double amount)
			throws SQLException, InsufficentFundsException, NoSQLResultsException {
		Account account1 = new Account();
		Account account2 = new Account();
		String sql = "Select * from accounts where accountholderId = (?) and accountid = (?)";
		PreparedStatement pstmt = connection.prepareStatement(sql);
		pstmt.setInt(1, id);
		pstmt.setInt(2, accId);
		ResultSet rs = pstmt.executeQuery();
		String sqlSecond = "Select * from accounts where accountholderId = (?) and accountid = (?)";
		PreparedStatement pstmtSecond = connection.prepareStatement(sqlSecond);
		pstmtSecond.setInt(1, id);
		pstmtSecond.setInt(2, transferId);
		ResultSet rsSecond = pstmtSecond.executeQuery();
		if (rs.first() && rsSecond.first()) {
			account1.setBalance(rs.getDouble("Balance"));
			account2.setBalance(rsSecond.getDouble("Balance"));
			if (account1.getBalance() - amount > 0) {
				account1.withdraw(amount);
				sql = "Update accounts set Balance = (?) Where accountholderid = (?) and accountID = (?)";
				pstmt = connection.prepareStatement(sql);
				pstmt.setDouble(1, account1.getBalance());
				pstmt.setInt(2, id);
				pstmt.setInt(3, accId);
				pstmt.executeQuery();
				account2.deposit(amount);
				sql = "Update accounts set Balance = (?) Where accountholderid = (?) and accountID = (?)";
				pstmt = connection.prepareStatement(sql);
				pstmt.setDouble(1, account2.getBalance());
				pstmt.setInt(2, id);
				pstmt.setInt(3, transferId);
				pstmt.executeQuery();
			} else
				throw new InsufficentFundsException();

		}
	}
}
